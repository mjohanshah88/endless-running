﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coin : MonoBehaviour
{
    private int addedScore = 5;

    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 8, 0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        transform.Rotate(0, 0, 100 * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameFlow.totalCoins += addedScore;
            StartCoroutine(Tutorial.Instance.UpdateScore(addedScore));


            //Debug.Log(gameFlow.totalCoins);
            Destroy(gameObject);
        }
    }
}
