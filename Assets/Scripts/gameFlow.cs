﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameFlow : MonoBehaviour
{
    public Transform tile1Obj;
    private Vector3 nextTileSpawn;
    public Transform obs1; //obstacle
    private Vector3 nextObs1Spawn;
    public Transform obs2;
    private Vector3 nextObs2Spawn;
    public Transform obs3;
    private Vector3 nextObs3Spawn;
    private int randX;
    public static int totalCoins = 0;
    public Transform coinObj;
    private Vector3 nextCoinSpawn;
    private int randChoice = 0;
    public Text changeText;
    public Text coinText;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        totalCoins = 0;
        nextTileSpawn.z = 45.6f; //next tile spawn start after the preset tile in scene
        StartCoroutine(spawnTile());
        StartCoroutine(onText());
    }

    // Update is called once per frame
    void Update()
    {
        displayTotalCoins();
    }

    IEnumerator spawnTile() //clone tile
    {
        yield return new WaitForSeconds(1);
        randX = Random.Range(-1, 2); // -1 until 1 (2 not included)
        nextObs1Spawn = nextTileSpawn;
        //nextObs1Spawn.y = .16f; //necessary if want to change y value
        nextObs1Spawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(obs1, nextObs1Spawn, obs1.rotation);

        nextTileSpawn.z += 7.6f; //added distance between each tile
        randX = Random.Range(-1, 2); // the 2nd obstacle put in this line so that there's no 2 obj in the same row
        nextObs2Spawn.z = nextTileSpawn.z;
        //nextObs2Spawn.y = .176f; //necessary if want to change y value
        nextObs2Spawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(obs2, nextObs2Spawn, obs2.rotation);

        //fix x position for 3rd obstacle
        if (randX == 0)
        {
            randX = 1;
        }
        else
        {
            if (randX == 1)
            {
                randX = -1;
            }
            else
            {
                randX = 0;
            }
        }

        randChoice = Random.Range(0, 2);
        if (randChoice == 0)
        {
            nextObs3Spawn.z = nextTileSpawn.z;
            //nextObs3Spawn.y = .151f; //necessary if want to change y value
            nextObs3Spawn.x = randX;
            Instantiate(obs3, nextObs3Spawn, obs3.rotation);
        }
        else
        {
            nextCoinSpawn.z = nextTileSpawn.z;
            nextCoinSpawn.y = .28f;
            nextCoinSpawn.x = randX;
            Instantiate(coinObj, nextCoinSpawn, coinObj.rotation);
        }

        nextTileSpawn.z += 7.6f;
        StartCoroutine(spawnTile());
    }

    IEnumerator onText()
    {
        yield return new WaitForSeconds(2);
        changeText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        changeText.gameObject.SetActive(false);
    }

    public void displayTotalCoins()
    {
        if (totalCoins > 0)
        {
            coinText.text = totalCoins.ToString();
            coinText.gameObject.SetActive(true);
        }
    }
}
