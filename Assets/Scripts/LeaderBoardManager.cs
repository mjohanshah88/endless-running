﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LeaderBoardManager : MonoBehaviour
{
    [Header("Leaderboard Props")]
    public Transform leaderboardGrid;
    public GameObject board;

    public List<PlayerScore> playerScoreList = new List<PlayerScore>();

    void Start()
    {
        StartCoroutine(FetchLeaderBoard());
    }

    public IEnumerator FetchLeaderBoard()
    {
        bool isNotFinishFetching = true;

        StartCoroutine(Tutorial.Instance.GetLeaderboard(data =>
        {
            int i = 0;
            foreach (var item in data.message.data)
            {
                string username = item.alias;
                string score = "";

                StartCoroutine(Tutorial.Instance.GetScore(username, (scoreObj) =>
                {
                    PlayerScore newPlayer = new PlayerScore();

                    score = scoreObj.message.score[0].value;

                    newPlayer.username = username;
                    newPlayer.score = int.Parse(score);

                    playerScoreList.Add(newPlayer);

                    i++;
                    if (i == data.message.data.Length)
                    {
                        isNotFinishFetching = false;
                    }
                }));
            }
        }));

        yield return new WaitWhile(() => isNotFinishFetching == true);

        RenderScore();
    }

    void RenderScore()
    {
        playerScoreList.Sort((p1, p2) => p2.score.CompareTo(p1.score));

        foreach (var player in playerScoreList)
        {
            Debug.Log(player.username);
            Debug.Log(player.score);

            GameObject _boardName = Instantiate(board, Vector3.one, Quaternion.identity, leaderboardGrid);
            _boardName.transform.GetChild(0).GetComponent<Text>().text = player.username;

            GameObject _boardRank = Instantiate(board, Vector3.one, Quaternion.identity, leaderboardGrid);
            _boardRank.transform.GetChild(0).GetComponent<Text>().text = player.score.ToString();
        }
    }

}