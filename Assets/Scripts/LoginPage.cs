﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LoginPage : MonoBehaviour
{
    public InputField inputAliasR;
    public InputField inputAliasL;
    public InputField inputFname;
    public InputField inputLname;
    public InputField inputId;

    public GameObject registerUI;
    public GameObject loginUI;

    public Text status;

    public static LoginPage Instance;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void registerButton()
    {
        StartCoroutine(Tutorial.Instance.registerNewPlayer(inputAliasR.text, inputId.text, inputFname.text, inputLname.text, status));

    }

    public void loginButton()
    {
        StartCoroutine(Tutorial.Instance.getPlayer(inputAliasL.text, status));

    }

    public void resetInput()
    {
        inputAliasR.text = "";
        inputAliasL.text = "";
        inputFname.text = "";
        inputLname.text = "";
        inputId.text = "";
    }

    public void switchButton()
    {
        if (registerUI.activeInHierarchy == true)
        {
            registerUI.SetActive(false);
            loginUI.SetActive(true);

            resetInput();
        }
        else if (loginUI.activeInHierarchy == true)
        {
            loginUI.SetActive(false);
            registerUI.SetActive(true);

            resetInput();
        }
    }
}
