﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class runningDude : MonoBehaviour
{
    private string laneChange = "n"; //lanechange = no by default
    private string midJump = "n"; //midJump = no by default

    public static Vector3 playerPos;
    public Text changeText;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
    }

    // Update is called once per frame
    void Update()
    {
        playerPos = transform.position;

        if ((Input.GetKey("a")) && (laneChange == "n") && (transform.position.x > -.9) && (midJump == "n")) //transform.position.x block player move to outside area
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-1, 0, 6);
            laneChange = "y";
            StartCoroutine(stopLaneCh());
        }

        if ((Input.GetKey("d")) && (laneChange == "n") && (transform.position.x < .9) && (midJump == "n"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(1, 0, 6);
            laneChange = "y";
            StartCoroutine(stopLaneCh());
        }

        if ((Input.GetKey("space")) && (midJump == "n") && (laneChange == "n"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 2, 6);
            midJump = "y";
            StartCoroutine(stopJump());
        }

        if (Input.GetKey("r"))
        {
            SceneManager.LoadScene(1);
        }

        if (Input.GetKey("l"))
        {
            SceneManager.LoadScene(2);
        }
        //Debug.Log(transform.position.z);
    }

    IEnumerator stopJump()
    {
        yield return new WaitForSeconds(.75f); // jump duration
        GetComponent<Rigidbody>().velocity = new Vector3(0, -2, 6);
        yield return new WaitForSeconds(.75f); // land duration
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        midJump = "n";
    }

    IEnumerator stopLaneCh()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        laneChange = "n";
        //Debug.Log(GetComponent<Transform>().position); //check current pos
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "obstacle")
        {
            //Debug.Log("ouch!"); //check collide with obstacle
            changeText.text = "GAME OVER\n'r' to restart";
            changeText.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }

        /*if (other.tag == "ramp")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 6, 6);
        }

        if (other.tag == "dropdown")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, -6, 6);
        }*/
    }

    private void OnTriggerExit(Collider other)
    {
        /*if (other.tag == "ramp")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        }

        if (other.tag == "dropdown")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 6);
        }*/
    }

}
