﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cleanUp : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < runningDude.playerPos.z - 7.6) //value to minus depend on tile z
        {
            Destroy(gameObject);
        }
    }
}
