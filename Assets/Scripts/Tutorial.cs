﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Tutorial : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "ef05c3f0c74120bd40323e85ac09de48";

    public delegate void cb(LeaderboardJSON data);
    public delegate void scoreCb(ScoreJson data);
    public static Tutorial Instance;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(getPlayer());
        //StartCoroutine(UpdateScore());
    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }



    public IEnumerator registerNewPlayer(string alias, string id, string fname, string lname, Text status)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/createPlayer" + "?token=" + token + "&alias=" + alias + "&id=" + id + "&fname=" + fname + "&lname=" + lname);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            status.text = "connection error";
        }

        Response res = new Response();
        res = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        if (res.message == "player_exists")
        {
            status.text = "Alias has been taken";
        }
        else
        {
            status.text = "Player Registered Successfully";
        }

        LoginPage.Instance.resetInput();

    }

    public IEnumerator getPlayer(string alias, Text status)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        Response res = new Response();
        res = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            status.text = "There was an error with the your connection";
        }

        if (res.message == "player_no_exists")
        {
            status.text = "Player Alias Not Exist, Please Register.";
        }
        else
        {
            PlayerPrefs.SetString("alias", alias);
            Debug.Log(alias);
            SceneManager.LoadScene(1);
        }

    }

    public IEnumerator UpdateScore(int score)
    {
        string alias = PlayerPrefs.GetString("alias");
        Debug.Log(alias);

        UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + alias + "&id=" + "met" + "&operator=add" + "&value=" + score);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }

    public IEnumerator GetScore(string alias, scoreCb data)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
        ScoreJson res = new ScoreJson();
        res = JsonUtility.FromJson<ScoreJson>(www.downloadHandler.text);

        data(res);
    }

    public IEnumerator GetLeaderboard(cb data)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token + "&id=" + "lead");

        yield return www.SendWebRequest();

        LeaderboardJSON res = new LeaderboardJSON();
        res = JsonUtility.FromJson<LeaderboardJSON>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            data(res);
        }
    }
}
