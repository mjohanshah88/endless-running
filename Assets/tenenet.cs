﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class tenenet : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "60072588c119bd1eea2942d52ce451f5";

    private string alias = "jojo";
    private string fname = "joe";
    private string lname = "jou";
    private string id = "5615";

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(registerNewPlayer());
        //StartCoroutine(getPlayer());
        StartCoroutine(UpdateScore());
        //StartCoroutine(UpdateLeaderboard());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator registerNewPlayer()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/createPlayer" + "?token=" + token + "&alias=" + alias + "&id=" + id + "&fname=" + fname + "&lname=" + lname);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }

    private IEnumerator getPlayer()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }

    private IEnumerator UpdateScore()
    {

        UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + "jojo" + "&id=" + "metric" + "&operator=add" + "&value=" + "20");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }

    private IEnumerator UpdateLeaderboard()
    {

        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token + "&id=" + "myleaderboard");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }
}